using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    void Update()
    {
        CoinRotation();
    }

    private void CoinRotation(){
        transform.eulerAngles += new Vector3(0, 1, 0);
    }


    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Player":
                GameManager.Instance.AddCoin();
                Destroy(gameObject);
                break;
            case "Coin":
                break;
            default:
                Destroy(_rigidbody);
                gameObject.transform.position += Vector3.up;
                break;
        }
        
    }
}
