using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState
{
    Completed,
    OnGoing,
    Started
}

public class GameManager : Singleton<GameManager>
{
    private int _coinsLeft;
    private float _timer = 0.0f;
    private GameState _gameState;

    [SerializeField] private Text _coinScore;
    [SerializeField] private Text _timerText;

    [SerializeField] private UnityEvent _restartEvent;

    private void Start()
    {
        if (_restartEvent == null)  _restartEvent = new UnityEvent();
        _coinScore.text = $"{_coinsLeft} Coins Left";
    }

    // Update is called once per frame
    void Update()
    {
        switch (_gameState)
        {
            case GameState.Completed:
                Restart();
                break;
            case GameState.OnGoing:
                Timer();
                break;
        }
    }
    public void AddCoin()
    {
        _coinsLeft--;
        if (_coinsLeft <= 0) {
            _gameState = GameState.Completed;
            _restartEvent.Invoke();
        };
        _coinScore.text = $"{_coinsLeft} Coins Left";
    }
    public void SpawnCoin()
    {
        _coinsLeft++;
        _coinScore.text = $"{_coinsLeft} Coins Left";
    }
    public void GameStart()
    {
        _gameState = GameState.OnGoing;
    }

    private void Timer()
    {
        _timer += Time.deltaTime;
        _timerText.text = string.Format("{0:00}:{1:00}:{2:00}", TimeSpan.FromSeconds(_timer).Hours, TimeSpan.FromSeconds(_timer).Minutes, TimeSpan.FromSeconds(_timer).Seconds);
    }
    private void Restart()
    {
        _timer = 0;
        _gameState = GameState.OnGoing;
        SceneManager.LoadScene(0);
    }
}
