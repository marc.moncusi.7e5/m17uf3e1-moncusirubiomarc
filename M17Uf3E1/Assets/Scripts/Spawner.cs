using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum SceneStatus
{
    Empty,
    CoinsInScene,
    Finished
}

public class Spawner : MonoBehaviour
{
    [SerializeField] private int _coinsTotal = 10;
    [SerializeField] private GameObject _coin;
    [SerializeField] private Renderer _floorRender;
    private SceneStatus _status;
    private float _floorXMin, _floorXMax;
    private float _floorZMin, _floorZMax;


    void Awake()
    {
        _floorXMin = _floorRender.bounds.center.x - _floorRender.bounds.size.x * 0.5f;
        _floorXMax = _floorRender.bounds.size.x * 0.5f + _floorRender.bounds.center.x;
        _floorZMin = _floorRender.bounds.center.z - _floorRender.bounds.size.z * 0.5f;
        _floorZMax = _floorRender.bounds.size.z * 0.5f + _floorRender.bounds.center.z;
    }

    void Update()
    {
        switch (_status)
        {
            case SceneStatus.Empty:
                SpawnCoins();
                GameManager.Instance.GameStart();
                _status = SceneStatus.CoinsInScene;
                break;
            case SceneStatus.CoinsInScene:
                break;
        }
    }

    void SpawnCoins()
    {
        for (int i = 0; i < _coinsTotal; i++)
        {
            Vector2 spawnPoint;
            do
            {
                spawnPoint = new Vector2(Random.Range(_floorXMin, _floorXMax),Random.Range(_floorZMin,_floorZMax));
            } while (!SpawnPossible(spawnPoint));
            Instantiate(_coin, new Vector3(spawnPoint.x,10,spawnPoint.y), new Quaternion(0, 0, 0, 0));
            GameManager.Instance.SpawnCoin();
        }
    }

    bool SpawnPossible(Vector2 position)
    {
        if (_floorXMin > position.x && position.x > _floorXMax)
        {
            return false;
        }
        if (_floorZMin > position.y && position.y > _floorZMax)
        {
            return false;
        }
        return true;
    }

}
