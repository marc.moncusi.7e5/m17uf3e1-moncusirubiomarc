# Third Person Controller Script Interpretation

### Awake

Inicializa referencia de la camara principal.

### Start

Inicializa algunas variables, IDs de animaciones, y resetea el timeout del salto y caida.

### Update

Actualiza el movimiento del jugador, el estado de "Grounded" y las fisicas de la gravedad.

### LateUpdate

Actualizado tras Update.

Actualiza la rotación de la camara.

### AssignAnimationIDs

Asigna IDs a los distintos parametros del Animator.

### GroundedCheck

Comprueba si el personaje esta tocando el suelo.

### CameraRotation

Dependiendo del input del jugador rota la camara.

### Move

Función para controlar el movimiento del jugador.

### JumpAndGravity

Aplica gravedad y fisicas del salto al movimiento del jugador y configurando la variable 'grounded' acorde.

### ClampAngle

Devuelve el angulo delimitandolo entre dos valores

### OnDrawGizomsSelected

Esta función permite ver los gizmos dentro de la pantalla de juego.

### OnFootstep

AnimationEvent -> es una clase de Unity que representa un evento que se puede añadir a una animación.

Esta función recoge la información del clip reproduciendose, y si el la influencia, weight, es suficientemente alta activa un audio aleatorio dentro de una col·lección.

### OnLand

AnimationEvent -> es una clase de Unity que representa un evento que se puede añadir a una animación.

Esta función recoge la información del clip reproduciendose, y si el la influencia, weight, es suficientemente alta activa el audio.