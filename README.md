# M17UF3E1- Primeres passes a Unity3D - Anàlisi de moviment

## Del pack de Standard Assets descarregat a classe, realitzar:

### Implementat al scene prefabricat de PlayGround

Una escena amb un nivell i un personatge res més emprant els assets descarregats.

L'escena ha de tenir monedes a agafar.

Si el jugador aconsegueix totes les monedes es reinicia el joc.

## També realitzar un document:

Document amb un dels scripts del personatge (preferiblement el que s'encarrega del moviment de la càmera) emprat en el nivell. Hau d'analitzar la programació comentant el comportament intern del script.

Es tracte de veure com s'utilitzen les càmeres, els vectors 3d, les animacions, les interaccions...

Podeu entregar l'escena en format package de Unity o en git

Formt del nom de projecte o de l'escena: M17UF3E1-MoncusiRubioMarc